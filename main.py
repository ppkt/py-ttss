import requests

import config

# base url for MPK endpoint
BASE_URL = 'http://91.223.13.70/internetservice/services/passageInfo/' \
           'stopPassages/stopPoint?stopPoint={}&mode=departure'

# base url for display
DISPLAY_URL = f'http://{config.DISPLAY_IP_ADDR}/cm?cmnd=DisplayText%20'
# base command to print text on display
DISPLAY_FMT = '[l{}c{}]{}'


def main():
    # fetch json with departure data for bus stop
    resp = requests.get(BASE_URL.format(config.BUS_PERON_ID))

    # our display has only 4 rows
    data = resp.json()['actual'][:4]

    # clear display
    requests.get(DISPLAY_URL + '[z]')

    for i, info in enumerate(data):
        # assign data to variables
        number = info['patternText']
        direction = info['direction']
        departure_mix = info['mixedTime']

        # prepare row with data, keep it under 20 chars to fit on display
        s = f'{number} {direction[:9]:<9} ' \
            f'{departure_mix.replace("%UNIT_MIN%", "m"):<5}'
        print(s)

        # prepare URL to update the display
        if i == 0:
            # that's odd but it seems that there is a bug in Tasmota software
            # and first row starts from 0 column, ignoring provided column
            s = ' ' + s
        url = DISPLAY_URL + DISPLAY_FMT.format(i, 1, s)
        requests.get(url)

        # wait for display update
        # time.sleep(0.1)


if __name__ == '__main__':
    main()
