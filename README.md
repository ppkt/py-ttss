# TTSS Display

It's a small project dedicated to display bus departure time from provided bus stop in Krakow. It's using data from [TTSS](http://www.ttss.krakow.pl/) to provide realtime data of bus departures. 

![poc](https://i.imgur.com/TJKKEoU.jpg)

It consists from 2 parts - a small "backend" written in Python to fetch data, format it and push to remote display and "frontend", in my case it's NodeMCU WiFi board (ESP8266) with Tasmota software and attached 20x4 LCD display.

## Backend

It's pretty straightforward, only requirements are Python 3.6+ and [Requests](https://2.python-requests.org) library (it's possible to use `urllib` instead but it's not supported right now)

### Installation

1. Fetch latest version
    ```bash
    $ git clone git@gitlab.com:ppkt/py-ttss.git
    $ cd py-ttss
    ```
2. (Optional) create dedicated virtualenv
    ```
    $ python -m venv .venv
    $ source .venv/bin/activate
    ```
2. Install requirements
    ```
    $ pip install -r requirements.txt
    ```
    Note: In some Linux distros it's possible to use packet manager instead of `pip`, e.g: `sudo apt install python3-requests`.

4. Create proper configuration file
    ```
    $ cp config.example.py config.py
    $ nano config.py
    ```

    Right now there are only two values to provide, an ID of bus peron which could be obtained from page: https://mpk.jacekk.net/map.html - zoom in to find bus peron - next to its name will be an ID number. For example: Teatr Słowackiego (324203) - `324203` is the ID.

    Second value is an IP address of display.

5. Run: 
    ```
    $ python3 main.py
    ```
    
5. (Optional) Add script to crontab / systemd to execute it periodically.

## Hardware

As mentioned earlier, project is using ESP8266 module and it's recommended to use dedicated hardware board with exposed at least 5 pins (+3.3V, +5.0V, GND, I2C SDA/SCL). Boards like Wemos D1 or Node MCU are perfect, especially because they have included USB-TTY converter so it's possible to program them directly from PC and power it up using mobile phone power supply.

Requirements:

* ESP8266 board (Wemos, Node MCU)
* 20x4 LCD Display (or other, compatible with HD44780)
* HD44780 to I2C converter (PCF8574)
* Logic level converter (ESP8266 is using 3.3V but display requires 5V)
* Ribbon cable to connect display with board
* Soldering iron

### Connection

* Solder I2C adapter to LCD display

* Solder I2C adapter to Logic Level Converter (you can attach it to goldpins):
    ```
    I2C   LLC
    
    GND - GND
    VCC - HV
    SDA - HV2
    SCL - HV1
    ```

* Solder ESP board to Logic Level Converter as described:
    
    Note: HV pin is also soldered to I2C converter
    ```
    ESP   LLC
    
    GND - GND
     5V - HV
     3V - LV
     D1 - LV1
     D2 - LV2
    ```

* Check if SCL pin is connected (via converter) to D1 and SDA to D2

* Connect ESP8266 to PC

### Flashing

In order to get device up and running you have to flash device with Tasmota firmware (version 6.5.0+ is supported). Detailed instructions are on [project's site](https://github.com/arendst/Sonoff-Tasmota/wiki/Wemos-D1-Mini). But in nutshell you have to:

1. Download [esptool](https://github.com/espressif/esptool) or `pip install esptool` 
2. Download [Tasmota firmware](https://github.com/arendst/Sonoff-Tasmota/releases) with display support (look for `sonoff-display.bin`)
3. [Flash firmware](https://github.com/arendst/Sonoff-Tasmota/wiki/Flashing#esptoolpy)
4. Run [Initial Configuration](https://github.com/arendst/Sonoff-Tasmota/wiki/Initial-Configuration) to connect module to WiFi network
5. Configure display

    1. Check [documentation](https://github.com/arendst/Sonoff-Tasmota/wiki/Commands) about commands
    2. Go to module's console webpage (e.g. http://192.168.1.157/cs)
    3. Configure module using following commands:

        `DisplayModel 1` - use I2C LCD driver

        `DisplayMode 0` - display text provided by user

        `DisplayCols 20` - use 20 columns

        `DisplayRows 4` - use 4 rows

    4. Test settings:

        `DisplayText Hello, World!`

    5. If everything is correct, update `config.py` file and run script. On display and in console there should be displayed bus departure times.

    6. If not, check [Displays](https://github.com/arendst/Sonoff-Tasmota/wiki/Displays)

Happy Hacking!
